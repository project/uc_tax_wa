<?php

/**
 * @file
 * Admin settings for uc_tax_wa.
 *
 * Copyright (C) 2008 by Jennifer Hodgdon, Poplar ProductivityWare LLC
 */


/**
 * Admin settings menu callback.
 */
function uc_tax_wa_admin_settings() {

  $form = array();

  // Set default tax rate.
  $form['uc_tax_wa_default_rate'] = array(
    '#title' => t('Default tax rate'),
    '#description' => t('The default tax rate to use if the delivery address is in Washington state but the rate lookup fails. Enter as a decimal, such as 9.8 for a 9.8% tax rate.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_tax_wa_default_rate', '0'),
  );

  // Set default location code.
  $form['uc_tax_wa_default_loc'] = array(
    '#title' => t('Default location code'),
    '#description' => t('The default tax location code to use if the delivery address is in Washington state but the rate lookup fails. This is a 4-digit number, obtained from the !dor. Note that providing this location does NOT override the default rate (they should agree).', array('!dor' => l('Department of Revenue', 'https://webgis.dor.wa.gov/taxratelookup/SalesTax.aspx'))),
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_tax_wa_default_loc', '0'),
  );

  // Configure where to show location code.
  $form['uc_tax_wa_show_loc'] = array(
    '#title' => t('Show location code'),
    '#description' => t('Set to Yes if you want to show the location code on the tax line item, so you and the customer can see what location was used.'),
    '#type' => 'radios',
    '#options' => array('0' => t('No'), '1' => t('Yes')),
    '#default_value' => variable_get('uc_tax_wa_show_loc', '0'),
  );

  // Wholesale users - exclude users with these role from tax.
  $form['uc_tax_wa_wholesale_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Wholesale roles, to exclude from tax'),
    '#description' => t('Exclude tax from any user who has one of the checked roles.'),
    '#multiple' => TRUE,
    '#options' => user_roles(TRUE),
    '#default_value' => variable_get('uc_tax_wa_wholesale_roles', array()),
  );

  // Define taxed product types.
  $form['uc_tax_wa_product_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Taxed product types'),
    '#description' => t('Product types to apply Washington state sales taxes to.'),
    '#multiple' => TRUE,
    '#options' => uc_product_type_names(),
    '#default_value' => variable_get('uc_tax_wa_product_types', array()),
  );

  // Taxed line items. Ripped from the uc_taxes module.
  $options = array();
  foreach (_uc_line_item_list() as $line_item) {
    if (!in_array($line_item['id'], array('subtotal', 'tax_subtotal', 'total'))) {
      $options[$line_item['id']] = $line_item['title'];
    }
  }

  $form['uc_tax_wa_line_items'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Taxed line items'),
    '#description' => t('Line items to apply Washington state sales taxes to.'),
    '#multiple' => TRUE,
    '#options' => $options,
    '#default_value' => variable_get('uc_tax_wa_line_items', array()),
  );

  return system_settings_form($form);
}


/**
 * Form for administrative lookup of a WA State Sales Tax rate.
 *
 * @see uc_tax_wa_admin_lookup_form_submit()
 */
function uc_tax_wa_admin_lookup_form() {

  $form['uc_tax_wa_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Street address'),
    '#required'      => TRUE,
    //'#default_value' => variable_get('uc_tax_wa_address', ''),
  );
  $form['uc_tax_wa_city'] = array(
    '#type'          => 'textfield',
    '#title'         => t('City'),
    '#required'      => TRUE,
    //'#default_value' => variable_get('uc_tax_wa_city', ''),
  );
  $form['uc_tax_wa_zip'] = array(
    '#type'          => 'textfield',
    '#title'         => t('ZIP'),
    '#required'      => TRUE,
    //'#default_value' => variable_get('uc_tax_wa_zip', ''),
  );
  $form['debug'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Debug output'),
    '#description'   => t('Displays complete response from DOR server.'),
    '#default_value' => FALSE,
  );
  $form['submit'] = array(
    '#type'          => 'submit',
    '#value'         => t('Submit'),
    //'#default_value' => variable_get('uc_tax_wa_zip', ''),
  );

  return $form;
}


/**
 * Submit handler for uc_tax_wa_admin_lookup_form().
 *
 * @see uc_tax_wa_admin_lookup_form()
 */
function uc_tax_wa_admin_lookup_form_submit($form, &$form_state) {

  $address = $form_state['values']['uc_tax_wa_address'];
  $city    = $form_state['values']['uc_tax_wa_city'];
  $zip     = $form_state['values']['uc_tax_wa_zip'];

  $rate = uc_tax_wa_get_rate($address, $city, $zip, $form_state['values']['debug']);

  drupal_set_message('<pre>Tax rate = ' .  $rate . '</pre>');
}
